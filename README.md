# MailToJson

This repo is about wrapper script around [Newsman's MailToJson](https://github.com/Newsman/MailToJson).

## How to use

This bash wrapper script converts all the `.eml` files in a specified directory to a `json` format with `.json` file extension.

Example usage (command line):
```bash
./converter.sh ~/Documents/mail/
```

## JSON Format

```yaml
json:
  headers:
    header_key1: value
    header_key2: value
  subject: "The email subject as utf-8 string"
  datetime: "2015-03-17 17:48:06"
  encoding: "utf-8"
  from:
    - { name: "Sender Name", email: "sender@email.com" }
  to:
    - { name: "Recipient Name", email: "recpient@email.com" }
    - { name: "Recipient Name 2", email: "recpient2@email.com" }
  cc:
    - { name: "Recipient Name", email: "recpient@email.com" }
    - { name: "Recipient Name 2", email: "recpient2@email.com" }
  parts:
    - { content_type: "text/plain", content: "body of this part", "headers": { "header_key1": value, "header_key2": value } }
    - { content_type: "text/html", content: "body of this part", "headers": { "header_key1": value, "header_key2": value } }
  attachments:
    - { filename": "invoice.pdf", content_type: "application/pdf", content: "base64 of binary data" }
    - { filename": "invoice2.pdf", content_type: "application/pdf", content: "base64 of binary data" }
```

# License

This code is released under [MIT license](https://github.com/Newsman/MailToJson/blob/master/LICENSE) by [Newsman App - Smart Email Service Provider](https://www.newsmanapp.com).

#!/bin/bash

# temporarily substitude spaces with "__delimeter__"
filelist=`find $1 -name "*.eml" | sed 's/ /__delimeter__/g'`

for file in ${filelist}; do
  cat "`echo $file | sed 's/__delimeter__/ /g'`" | \
    python2.7 mailtojson.py -p > \
    "`echo "${file}" | sed -e 's/__delimeter__/ /g' -e 's/\.eml/\.json/g'`"
done
